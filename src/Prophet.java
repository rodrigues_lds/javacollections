/**
 * PROPHET CLASS
 * This class defines the properties and behavior of the prophet class.
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-09-19
 */
public class Prophet implements Comparable<Prophet> {

    private int order;
    private String name;

    /**
     * Non-default constructor
     *
     * @param order of the prophet in this dispensation
     * @param name  of the prophet
     */
    public Prophet(int order, String name) {
        this.order = order;
        this.name = name;
    }

    /**
     * This function sets the output format of this class.
     *
     * @return the string to be output.
     */
    public String toString() {
        return "Prophet #" + this.order + " is " + this.name;
    }

    /**
     * This function implements the comparator interface and sort the collections.
     *
     * @param o is the object to be compared
     * @return the compared order.
     */
    @Override
    public int compareTo(Prophet o) {
        return this.order - o.order;
    }
}
