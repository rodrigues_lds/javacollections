import java.util.*;

/**
 * WEEK 02 - JAVA COLLECTIONS
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-09-19
 * <p>
 * Write a program that incorporates a list, a queue, a set, and a tree.
 * Your program should showcase the unique features of these different types of collections.
 * Your program should demonstrate you know how to use generics with your collections.
 * Implement the Comparator interface and show how to sort one of your collections.
 */

public class JavaCollections {

    /**
     * This method defines the entry point for the program.
     *
     * @param args to be passed to the main() method
     */
    public static void main(String[] args) {

        String[] elements = {"b", "b", "C", "C", "A", "A"};

        System.out.println("\n****** LIST ******");
        try {
            List list = new ArrayList();
            // Adding items to the list (as listed in the elements array b b C C A A)
            Collections.addAll(list, elements);
            // Sorting the list by using the Comparator interface with Lambda Expression.
            // I could also use list.sort(String.CASE_INSENSITIVE_ORDER) to sort the list.
            // Ps. I have implemented another example of comparator interface inside the class Prophet.
            Collections.sort(list, (Comparator<String>) (s1, s2) -> s1.compareTo(s2));
            for (Object obj : list) {
                System.out.println((String) obj);
            }
            // Result - Output the list as it was inserted (maintains elements in insertion order. A A C C b b)
        } catch (ArrayIndexOutOfBoundsException ex) {
            // It catches index out of bounds if tyring to access an item by nonexistent index. (Ex. list.get(6);)
            System.out.println("Index is out of bounds.");
        } catch (Exception ex) {
            // It catches general errors when accessing items in the list.
            System.out.println("Something went wrong with your list.");
        }

        System.out.println("\n****** QUEUE ******");

        try {
            Queue queue = new PriorityQueue();
            // Adding items to the list (as listed in the elements array b b C C A A)
            Collections.addAll(queue, elements);
            Iterator iterator = queue.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
            // Result - Output elements prior to processing. It does not necessarily hold elements in FIFO. (A C A b C b)
        } catch (ArrayIndexOutOfBoundsException ex) {
            // It catches index out of bounds if tyring to access an item by nonexistent index. (Ex. list.get(6);)
            System.out.println("Queue index is out of bounds.");
        } catch (Exception ex) {
            // It catches general errors when accessing items in the list.
            System.out.println("Something went wrong with your queue list.");
        }

        System.out.println("\n****** SET ******");
        try {
            Set set = new TreeSet();
            // Adding items to the list (as listed in the elements array b b C C A A)
            Collections.addAll(set, elements);
            for (Object obj : set) {
                System.out.println((String) obj);
            }
            // Result - It does not allow duplicate elements and it automatically sorts the list where CAPITALIZED letters come first. (A C b)
        } catch (ArrayIndexOutOfBoundsException ex) {
            // It catches index out of bounds if tyring to access an item by nonexistent index. (Ex. list.get(6);)
            System.out.println("Set index is out of bounds.");
        } catch (Exception ex) {
            // It catches general errors when accessing items in the list.
            System.out.println("Something went wrong with your Set List.");
        }

        System.out.println("\n****** TREE ******");
        int counter = 0;
        // It creates a Key value pairs (Key and Value).
        TreeMap<Integer, String> treeMaps = new TreeMap<Integer, String>();
        try {
            // Adding items to the tree (as listed in the elements array b b C C A A)
            for (String s : elements) {
                treeMaps.put(++counter, s);
            }
            // Iteration over TreeMap.entrySet() - Mapping the elements in the tree in order to iterate. (key #n + Value)
            // Iterate over key-value pair using getKey() and getValue() methods of Map.Entry
            Set<Map.Entry<Integer, String>> entries = treeMaps.entrySet();
            for (Map.Entry<Integer, String> entry : entries) {
                System.out.println("Key:" + entry.getKey() + " Value: " + entry.getValue());
            }
            // Result - The Key and the Value (Key: 1 Value b, Key: 2 Value b, Key: 3 Value C, etc)

            // Replace the values based on the key (key: 6 Value will receive b ... Key: 1 will receive A.)
            // In this case, the loop is back forward to take advantage of the counter variable value.
            // It could be replaced manually (treeMaps.put (3, "F"), where 3 is the key while "F" the value;
            for (String s : elements) {
                treeMaps.put(counter--, s); // Accessing the value by its key (counter)...
            }
            System.out.println("\nAfter changing the values in each Key:");
            for (Map.Entry<Integer, String> entry : entries) {
                System.out.println("Key:" + entry.getKey() + " Value: " + entry.getValue());
            }
            // Result - The Key and the Value (Key: 1 Value A, Key: 2 Value A, Key: 3 Value C, etc)
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Tree index is out of bounds.");
        } catch (Exception ex) {
            System.out.println("Something went wrong with your Tree List.");
        }

        // Happy path - Adding a list of Prophets and their calling sequence.
        System.out.println("\n****** GENERIC ******");
        List<Prophet> genericList = new LinkedList<Prophet>();
        // Adding the object Prophet class in random order in the list.
        genericList.add(new Prophet(4, "Wilford Woodruff"));
        genericList.add(new Prophet(2, "Brigham Young"));
        genericList.add(new Prophet(6, "Joseph F. Smith"));
        genericList.add(new Prophet(1, "Joseph Smith Jr"));
        genericList.add(new Prophet(5, "Lorenzo Snow"));
        genericList.add(new Prophet(3, "John Taylor"));
        // Happy path - Sorting the collection (Prophets) by their calling sequence.
        // The prophet class implements the comparable interface to sort it by sequence.
        Collections.sort(genericList); // It calls the compareTo method in the Prophet class through the Comparable interface.
        for (Prophet prophet : genericList) {
            System.out.println(prophet);
        }
        // Result - Output the prophet list by order according to time they were called (1 Joseph, 2 B. Young, etc).
    }
}